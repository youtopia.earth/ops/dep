module gitlab.com/ytopia/ops/dep

go 1.13

require (
	github.com/google/go-jsonnet v0.15.0
	github.com/imdario/mergo v0.3.9
	github.com/kvz/logstreamer v0.0.0-20150507115422-a635b98146f0
	github.com/mitchellh/mapstructure v1.3.0
	github.com/opencontainers/runc v0.1.1
	github.com/pkg/errors v0.8.0
	github.com/sirupsen/logrus v1.5.0
	github.com/sparrc/go-ping v0.0.0-20190613174326-4e5b6552494c
	github.com/spf13/cobra v1.0.0
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.6.3
	github.com/yosuke-furukawa/json5 v0.1.1
	golang.org/x/net v0.0.0-20190522155817-f3200d17e092
)
