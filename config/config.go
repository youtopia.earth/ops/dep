package config

import "time"

func NewConfig() *Config {
	config := &Config{}
	return config
}

// Config struct
type Config struct {
	LogLevel       string `mapstructure:"LOG_LEVEL" json:"log_level"`
	LogType        string `mapstructure:"LOG_TYPE" json:"log_type"`
	LogForceColors bool   `mapstructure:"LOG_FORCE_COLORS" json:"logs_force_colors"`
	CWD            string `mapstructure:"CWD" json:"cwd"`

	Retry    string            `mapstructure:"RETRY" json:"retry,omitempty"`
	Timeout  time.Duration     `mapstructure:"TIMEOUT" json:"timeout,omitempty"`
	Delay    time.Duration     `mapstructure:"DELAY" json:"delay,omitempty"`
	CleanEnv bool              `mapstructure:"CLEAN_ENV" json:"cleanEnv,omitempty"`
	Env      map[string]string `mapstructure:"ENV" json:"env,omitempty"`
}
