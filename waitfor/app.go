package waitfor

import (
	"gitlab.com/ytopia/ops/dep/config"
)

type App interface {
	GetConfig() *config.Config
}
