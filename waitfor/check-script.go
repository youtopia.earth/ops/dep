package waitfor

import (
	"context"
	"os/exec"
	"syscall"

	"github.com/sirupsen/logrus"

	"gitlab.com/ytopia/ops/dep/tools"
)

func CheckScript(script []string, env []string, ctx context.Context) bool {
	contextFields := logrus.Fields{
		"script": script,
	}
	hookFunc := func(cmd *exec.Cmd) error {
		cmd.Env = append(cmd.Env, env...)
		go func(cmd *exec.Cmd) {
			select {
			case <-ctx.Done():
				if cmd.Process == nil {
					return
				}
				if pgid, err := syscall.Getpgid(cmd.Process.Pid); err == nil {
					syscall.Kill(-pgid, syscall.SIGKILL)
				}
			}
		}(cmd)
		return nil
	}
	if err := tools.RunCmd(script, contextFields, hookFunc, ctx); err != nil {
		return false
	}
	return true
}
