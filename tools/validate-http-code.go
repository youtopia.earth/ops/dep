package tools

import (
	"regexp"
)

func ValidateHttpCode(code string) bool {
	validHttpCode, _ := regexp.Compile("^([1-5][0-9][0-9])$")
	return validHttpCode.MatchString(code)
}
